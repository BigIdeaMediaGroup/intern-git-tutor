# Big Idea Intern Git Tutor

This is a safe environment to learn the basic principles of an effective git 
workflow.

1. Check out the issues on the issues tab in gitlab
2. Fork the project by click the fork button
3. Clone the fork with `$ git clone _forkUrl_` `$ git clone git@gitlab.com:TheGrayPilgrim/intern-git-tutor.git`
4. Add the upstream remote with `$ git remote add upstream _Url_` `$ git remote add upstream git@gitlab.com:BigIdeaMediaGroup/intern-git-tutor.git`
5. Make a new branch with `$ git checkout -b _branchName_` `$ git checkout -b READMEUpdate`
6. Make all changes to the new branch  
7. Add all the changed files with `$ git add _urlTofiles_` `$ git add README.md` 
8. Commit the changes with `$ git commit`
9. Type in the commit at the top and close out the commit adder.  
10.   The first line should be short that clearly states what was added.
11.   Then hit return/enter two times and added any thing else needed to explain the changes.
12.   Code needs to be in wrapped in ''' '''
13.   Link it the the issue by added #issueNumber. Add close before it if the issue is done.
14. Push with `$ git push`
15. go to gitlab and hit the merge button
16. Add any tags and commits needed
17. Go back to the master branch with `$ git checkout -b master`
18. After it is accepted, fetch the new upstream with `$ git fetch upstream`
19. Merge upstream's changes with master with `$ git merge upstream/master`
20. push master's changes with origin/master with `$ git push origin master`
21. Optionally remove any old branches with `$ git branch -d branchName`
22. To make more changes fetch from upstream with `$ git pull`
23. Merge upsteam's changes with master with `$ git merge upstream/master`
24. Look at the issues in gitlab
25. Go back to step #5 

